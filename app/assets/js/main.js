// global variables
	var globalWrapper = $('.wrapper'),
		globalContent = $('.content'),
		zoomPicture,
		zoomTarget,
		currentPage,
		screenSize = window.innerWidth;
$( document ).ready(function() {

	// header manipulations
	if ($('.user-trigger').length>0) {
		// search
		$('.user-trigger.--search').on('click', function() {
			$(this).toggleClass('in');
			if ($(this).hasClass('in')) {
				$('.user-trigger.--navigation').removeClass('in');
				if (window.innerWidth>991) {
					$('.dc-wrap.b_navigation').fadeIn(0);
				}
				else {
					$('.dc-wrap.b_navigation').fadeOut(0);
				}
				$('.dc-wrap.b_search').fadeIn(0);
			}
			else {
				$('.dc-wrap.b_search').fadeOut(0);
			}
			return false;
		});
		// navigation
		$('.user-trigger.--navigation').on('click', function() {
			$(this).toggleClass('in');
			if ($(this).hasClass('in')) {
				$('.user-trigger.--search').removeClass('in');
				$('.dc-wrap.b_search').fadeOut(0);
				$('.dc-wrap.b_navigation').fadeIn(0);
			}
			else {
				$('.dc-wrap.b_navigation').fadeOut(0);
			}
			return false;
		});
	}
	// social icons slider
	if ($('.social-list').length>0) {
		var countElements = $('.social-list li').length,
			widthElements = $('.social-list li').width();
			$('.social-list').css({
				'width': countElements*widthElements+30*countElements
			});
		if (window.innerWidth && window.innerWidth <= 767) {
			var countElements = $('.social-list li').length,
				widthElements = $('.social-list li').width();

			$('.social-list').css({
				'width': countElements*widthElements+20*countElements-20
			});
		}
	}
	// main slider
	if ($('.main-slider').length>0) {
		$('.main-slider').bxSlider({
			auto: false,
			speed: 1000,
			pagerCustom: '.main-slider-pager',
			controls: true
		})
	}
	// gallery zoom & img switch
	if ($("#main-zoom").length>0) {
		$("#main-zoom").elevateZoom({
			lensSize: 150,
			zoomType: "lens",
			cursor: 'pointer',
			lensShape : "round",
			gallery:'goods-list',
			galleryActiveClass: 'active'
		});
	};
	// color picker
	if ($('.color-picker').length>0) {
		$('.current-color').on('click', function(){
			if (!$(this).hasClass('toggled')) {
				$('.current-color').removeClass('toggled').closest('.color-picker').find('.color-list').slideUp();
				$(this).addClass('toggled').closest('.color-picker').find('.color-list').slideDown();
				// mobile color list
				if (window.innerWidth<992) {

					var mColorList = $('.current-color.toggled + .color-list li');
					var mlistCustomize = $('.current-color.toggled + .color-list');
					var colorWdth = mColorList.width();
					var colorLgnt = mColorList.length;

					mlistCustomize.wrap('<div class="list-wrap"></div>');
					mlistCustomize.css({
						'width': colorWdth*colorLgnt/2+'px'
					});
				}
				// color pick
				$('.color-list [data-color]').on('click', function(){

					var selectedPicker = $('.current-color.toggled');
					var colorData = $(this).attr('data-color');
					var nameColorData = $(this).attr('data-name');

					$(selectedPicker).find('.color-preview').attr('data-color',colorData);
					$(selectedPicker).find('.color-name').html(nameColorData);
				});
			} else {
				$(this).removeClass('toggled').closest('.color-picker').find('.color-list').slideUp();
			}
		});
	}
	// product controls action
	if ($('.product-controls ').length>0) {
		$('.ctrl__share').on('click',function() {
			$(this).toggleClass('in modal')
			if ($(this).hasClass('in modal')) {
				$('.control-modal').fadeIn();
			} else {
				$('.control-modal').fadeOut();
			}
			return false;
		});
	}
	// form action
	if ($('.form-group').length>0) {
		$('.form-control').focus(function(){
			$(this).closest('.form-group').addClass('form-manipulation');
		});
		$('.form-control').focusout(function(){
			$(this).closest('.form-group').removeClass('form-manipulation');
		});
	}
	// tabs
	if ($('.tabs-module').length>0) {
		$('.tabs-module .path-link').on('click', function() {
			$('.tabs-module .path-link').removeClass('active');
			var currentTab = $(this).addClass('active').attr('data-tab');
			$('.tabs-module .tab-content').removeClass('active');
			$('#'+currentTab).addClass('active');
			return false;
		});
	}
	// media print
	$('.btn-print').on('click', function(){
		window.print();
	});
	// resize
	$(window).resize(resize);
	// goods page pagination manipulation
	if ($('.no-reload-parent').length>0) {
		currentPage = null;
		pagerManipulation(1);
		// paginationClick();
		imageSwap();
		// $('.paginator li').on('click', function(){
		// 	$('.paginator li').removeClass('active');
		// 	if (!$(this).hasClass('active')) {
		// 		var currentPagination = $(this).addClass('active');
		// 		var pageNumber = $(currentPagination).find('.pager-btn').attr('data-pagination');
		// 		// передача текущей страницы в функцию обработчик
		// 		pagerManipulation(pageNumber);
		// 	}
		// 	return false;
		// });
	}

	// end
});

function pagerManipulation(pageIndex) {
	console.log(pageIndex);
	if (currentPage==pageIndex) {
		return;
	}

	currentPage = pageIndex;
	$('.goods-listing').html('');

	var myCount = paginationCount();

	for(var i=(pageIndex-1)*myCount; i<((pageIndex-1)*myCount)+myCount && i<imagesArray.length; i++) {
		// console.log(imagesArray[i]);
		$('.goods-listing').append('<li class="item-listing-wrap"><div class="listing-item"><img src="'+imagesArray[i][0]+'"></div></li>');
		$('.goods-listing li:first').addClass('active');
	}
	redrawPagination();
}

function paginationCount() {
	screenSize = window.innerWidth;
	if (screenSize<=991) {
		return 6;
	}
	else if(screenSize>=992) {
		return 9;
	}
	else if(screenSize<=767) {
		return imagesArray.length;
	}
}

function redrawPagination() {
	$('.no-reload-parent .paginator').html('');
	var myCount = paginationCount();
	var totalPages = Math.ceil(imagesArray.length/myCount);
	for (var i = 1; i<=totalPages; i++) {
		$('.paginator-wrapper .paginator').append('<li><a href="" class="pager-btn td-none" data-pagination="'+i+'">'+i+'</a></li>');
	};
	$('.paginator-wrapper li').each(function(index){
		if (currentPage-1==index) {
			$(this).addClass('active');
		}
	});
	paginationClick();
	// TODO рассчитать группировку элементов при разрешениии экрана
}

function paginationClick() {
	$('.paginator li').on('click', function(){
		$('.paginator li').removeClass('active');
		$(this).addClass('active');
			// передача текущей страницы в функцию обработчик
		pagerManipulation($(this).find('a').attr('data-pagination'));
		return false;
	});
}

function imageSwap() {
	$('.item-listing-wrap').on('click', function(){
		$('.item-listing-wrap').removeClass('active');
		$(this).addClass('active');

	});
}

function resize() {

}
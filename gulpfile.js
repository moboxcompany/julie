'use strict';

var gulp = require('gulp'),
	jade = require('gulp-jade'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	rimraf = require('gulp-rimraf'),
	connect = require('gulp-connect'),
	imagemin = require('gulp-imagemin'),
	minifyCSS = require('gulp-minify-css'),
	concatCSS = require('gulp-concat-css'),
	sourcemaps = require('gulp-sourcemaps'),
	pngquant = require('imagemin-pngquant'),
	// livereload = require('gulp-livereload'),
	autoprefixer = require('gulp-autoprefixer');

// project path
var path = {
	// build path
	build: {
		html: './build',
		css: './build/css',
		js: './build/js',
		images: './build/images'
	},
	// sources path
	src: {
		jade: './app/templates/pages/**/*.jade',
		sass: './app/assets/stylesheets/sass/**/*.sass',
		js: './app/assets/js/**/*.js',
		images: './app/assets/images/**/*.*'
	},
	// watcher path
	watch: {
		jade: './app/templates/**/*.jade',
		sass: './app/assets/stylesheets/sass/**/*.sass',
		js: './app/assets/js/**/*.js',
		images: './app/assets/images/**/*.*'
	},
	// cleaner path
	clean: {
		clean: './build'
	}
};


//! TASKS !//
gulp.task('default', ['connect', 'sass', 'jade', 'images', 'js', 'watch']);

//	!LESS TASK!	//
gulp.task('sass', function() {
	return gulp.src(path.src.sass)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 50 versions'],
			cascade: false
		}))
		// .pipe(minifyCSS())
		.pipe(concatCSS('main.css'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css));
});

//	!JADE TASK!	//
gulp.task('jade', function() {
	return gulp.src(path.src.jade)
		.pipe(jade({
			pretty: true
		}))
		.pipe(gulp.dest(path.build.html));
});

//	!JS TASK!	//
gulp.task('js', function() {
	return gulp.src(path.src.js)
		.pipe(concat('main.js'))
		.pipe(gulp.dest(path.build.js));
});

//	!IMAGES TASK!	//
gulp.task('images', function () {
	return gulp.src(path.src.images)
		.pipe(imagemin({
			progressive: true,
			optimizationLevel: 7,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest(path.build.images));
});

//	!CONNECT TASK!	//
gulp.task('connect', function() {
	connect.server({
		port: 1337,
		root: './build'
		// livereload: true
	});
});

//	!WATCH TASK!	//
gulp.task('watch', function() {
	gulp.watch([path.watch.jade, path.watch.sass, path.watch.images, path.watch.js], ['sass', 'jade', 'images', 'js']);
});

